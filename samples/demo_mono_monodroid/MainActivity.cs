using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Content.PM;

using PlayScript.Application.Android;

namespace demo.mono.monodroid
{
	public class MainActivity : Activity
	{
		GLView1 view;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
				
			ActivityInfo ai = PackageManager.GetActivityInfo (this.ComponentName, PackageInfoFlags.Activities | PackageInfoFlags.MetaData);
			Bundle metaData = ai.MetaData;
			String applicationClass = metaData.GetString ("playscript.applicationClass");
			Type type = Type.GetType ("_root." + applicationClass);
			view = new GLView1 (this, type);
			SetContentView (view);
		}

		protected override void OnPause ()
		{
			// never forget to do this!
			base.OnPause ();
			view.Pause ();
		}

		protected override void OnResume ()
		{
			// never forget to do this!
			base.OnResume ();
			view.Resume ();
		}
	}
}


